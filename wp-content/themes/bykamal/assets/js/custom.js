// navbar effect after scroll
$(window).scroll(function() {
    if ($(document).scrollTop() > 70) {
        $('nav.navbar').addClass('navbar-dark bg-dark').removeClass('navbar-light');
    } else {
        $('nav.navbar').addClass('navbar-light').removeClass('navbar-dark bg-dark');
    }
});


$( document ).ready(function() {
    const li = $('#menu-top-menu > li');
    const a = $('#menu-top-menu > li > a');
    const menuContainer = $('.menu-top-menu-container');

    menuContainer.addClass('text-center collapse navbar-collapse').attr('id', 'navbarSupportedContent');
    li.addClass('nav-item');
    a.addClass('nav-link');
});