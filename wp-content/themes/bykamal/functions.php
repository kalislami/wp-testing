<?php

function load_css()
{
    wp_register_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), false, 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('style', get_template_directory_uri() . '/assets/css/custom.css', array(), false, 'all');
    wp_enqueue_style('style');
}
add_action('wp_enqueue_scripts', 'load_css');


function include_jquery()
{
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery-3.3.1.min.js', '', 1, true);
    add_action('wp_enqueue_scripts', 'jquery');
}
add_action('wp_enqueue_scripts', 'include_jquery');


function load_js()
{
    wp_register_script('popperjs', get_template_directory_uri() . '/assets/js/popper.min.js', '', 1, true);
    wp_enqueue_script('popperjs');

    wp_register_script('bootstrapjs', get_template_directory_uri() . '/assets/js/bootstrap.min.js', '', 1, true);
    wp_enqueue_script('bootstrapjs');

    wp_register_script('customjs', get_template_directory_uri() . '/assets/js/custom.js', '', 1, true);
    wp_enqueue_script('customjs');
}
add_action('wp_enqueue_scripts', 'load_js');


add_theme_support('menus');

register_nav_menus(

    array(
        'top-menu' => __('Top Menu', 'theme'),
        // 'footer-menu' => __('Footer Menu', 'theme')
    )

);

add_theme_support('post-thumbnails');

add_image_size('smallest', 300, 300, true);
add_image_size('largest', 800, 800, true);

?>