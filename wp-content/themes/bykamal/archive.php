<?php get_header(); ?>
    
<div class="container my-5 pt-5">
    <h1> <?php single_cat_title(); ?> </h1>

    <div class="row">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <div class="col-lg-6">
            <div class="card mb-4">
                <div class="card-body">
                    <?php if (has_post_thumbnail()): ?>
                        <img src="<?php the_post_thumbnail_url(array(150, 150)); ?>" class="rounded" />
                    <?php endif; ?>

                    <h3> <?php the_title(); ?> </h3>
                    <?php the_excerpt(); ?>
                    <a href="<?php the_permalink(); ?>" class="btn btn-primary"> Read More </a>
                </div>
            </div>
        </div>

        <?php endwhile; endif; ?>
    </div>
</div>

<?php get_footer(); ?>