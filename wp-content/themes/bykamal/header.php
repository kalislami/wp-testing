<!DOCTYPE html>
<html>
    <head>

        <?php wp_head(); ?>

    </head>
<body <?php body_class(); ?> >
    
    <nav class="navbar navbar-expand-lg fixed-top navbar-light">
        <div class="container">
            <a class="navbar-brand" href="/wp-testing">
                Logo
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- <div class="text-center collapse navbar-collapse" id="navbarSupportedContent"> -->

                <?php wp_nav_menu(

                    array(
                        'theme_location' => 'top-menu',
                        'menu_class' => 'navbar-nav ml-auto'
                    )

                ); ?>
                <!-- <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" 
                            href="#" id="navbarDropdown" role="button" 
                            data-toggle="dropdown" aria-haspopup="true" 
                            aria-expanded="false"
                        >
                            bahasa
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">
                                english
                            </a>
                        </div>
                    </li>
                </ul> -->
            <!-- </div> -->
        </div>
    </nav>