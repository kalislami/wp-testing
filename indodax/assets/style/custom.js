// navbar effect after scroll
$(window).scroll(function() {
    if ($(document).scrollTop() > 100) {
        $('nav.navbar').addClass('scrolled');
    } else {
        $('nav.navbar').removeClass('scrolled');
    }
});

// button password
$( "#btn-password" ).click(function() {
    const i = $('#btn-password > i');
    const input = $('#inputPassword');

    if (i.hasClass('fa-eye-slash')) {
        i.removeClass('fa-eye-slash').addClass('fa-eye');
        input.attr('type', 'text');
    } else {
        i.removeClass('fa-eye').addClass('fa-eye-slash');
        input.attr('type', 'password');
    }
});

$( document ).ready(function() {
    $.get('https://api.rawg.io/api/games/2', function( data ) {
        console.log(data.name)
    });
});